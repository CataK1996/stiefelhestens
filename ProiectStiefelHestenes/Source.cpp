#include<iostream>
#include<math.h>
#include<fstream>
#include<cstdlib>

using namespace std;

double an(double x[], int n)
{
	double s = 0;
	for (int i = 1;i <= n;i++)
		s += x[i] * x[i];
	return s;
}

void pro(double a[][10], double z[], double r[], int n)
{
	for (int i = 1;i <= n;i++)
	{
		r[i] = 0;
		for (int j = 1;j <= n;j++)
			r[i] += a[i][j] * z[j];
	}
}

double anorm2(double x[10], int n)
{
	double s = 0;
	for (int i = 1;i <= n;i++)
		s += fabs(x[i]);
	return s;
}

void SHMGC(double a[][10], int n, double x[10], double b[10], double eps, double u[10], double r[10], double r1[10])
{
	double s1 = 0;
	for (int i = 1;i <= n;i++)
	{
		double s = 0;
		for (int j = 1;j <= n;j++)
		{
			s += a[i][j] * x[j];
			r[i] = s - b[i];
			s1 += r[i] * r[i];
		}
	}
	pro(a, r, r1, n);
	double q = 0;
	for (int i = 1;i <= n;i++)
		q += r[i] * r1[i];
	for (int i = 1;i <= n;i++)
	{
		x[i] = x[i] - s1*r[i] / q;
		u[i] = -r[i];
	}
	for (int i = 1;i <= n;i++)
	{
		double s = 0;
		for (int j = 1;j <= n;j++)
		{
			s += a[i][j] * x[j];
			r1[i] = s - b[i];

		}
	}
loop:
		double s = an(r1, n) / an(r, n);
		for (int i = 1;i <= n;i++)
			u[i] = -r1[i] + s*u[i];
		pro(a, u, r, n);
		double al = 0;
		for (int i = 1;i <= n;i++)
			al += u[i] * b[i];
		al = an(r1, n) / al;
		for (int i = 1;i <= n;i++)
		{
			x[i] = x[i] + al*u[i];
			r[i] = r1[i];
			r1[i] = r1[i] + al*b[i];
		}
		/*for (int i = 1;i <= n;i++)
			cout << x[i] << "\t" << endl;*/
		if (anorm2(r1, n) > eps)
			goto loop;
}

int main()
{
	ifstream f("nr.txt");
	double a[10][10], x[10], b[10], r[10], u[10], r1[10], eps, n;
	cout << "eps = ";
	cin >> eps;
	f >> n;
	for (int i = 1;i <= n;i++)
		for (int j = 1;j <= n;j++)
			f >> a[i][j];
	for (int i = 1;i <= n;i++)
		f >> b[i];
	for (int i = 1;i <= n;i++)
		f >> x[i];
	SHMGC(a, n, x, b, eps, u, r, r1);
	for (int i = 1;i <= n;i++)
		cout << x[i] << "\n";

	system("PAUSE");
}